<?php
/*************************************************************************************
 * c1.php
 * ----------------
 * Authors: Vladislav Rastrusny (FractalizeR@yandex.ru), 
 *          Kirill Krasnov <krasnovforum@gmail.com> (http://www.kraeg.ru)
 * Copyright: (c) 2009 Kirill Krasnov, Vladislav Rastrusny, 
 *                     Nigel McNie (http://qbnz.com/highlighter)
 * Release Version: 1.0.0.1
 * Date Started: 2009/12/11
 *
 * 1C language file for GeSHi.
 *
 * -------------------------
 *
 *************************************************************************************
 *
 *     This file is part of GeSHi.
 *
 *   GeSHi is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   GeSHi is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with GeSHi; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ************************************************************************************/

$language_data = array (
	'LANG_NAME' => '1C',
	'COMMENT_SINGLE' => array(1 => '//'),
	'COMMENT_MULTI' => array(),
	'CASE_KEYWORDS' => GESHI_CAPS_NO_CHANGE,
	'QUOTEMARKS' => array('"'),
	'ESCAPE_CHAR' => '\\',
	'NUMBERS' =>
		GESHI_NUMBER_INT_BASIC | GESHI_NUMBER_FLT_SCI_ZERO,
	'KEYWORDS' => array(
		1 => array('если','Если', 'ЕСЛИ', 'If','тогда', 'Тогда', 'ТОГДА', 'Then', 'иначеесли',
					'ИначеЕсли', 'Иначеесли', 'ИНАЧЕЕСЛИ', 'ElseIf', 'иначе', 'Иначе', 'ИНАЧЕ', 'Else',
					'конецесли', 'КонецЕсли', 'Конецесли', 'КОНЕЦЕСЛИ', 'EndIf', 'для', 'Для', 'ДЛЯ',
					'For', 'каждого', 'Каждого', 'КАЖДОГО', 'Each', 'из', 'Из', 'ИЗ', 'In', 'по', 'По',
					'ПО', 'To', 'пока', 'Пока', 'ПОКА', 'While', 'цикл', 'Цикл', 'ЦИКЛ', 'Do',
					'конеццикла', 'КонецЦикла', 'Конеццикла', 'КОНЕЦЦИКЛА', 'EndDo', 'процедура',
					'Процедура', 'ПРОЦЕДУРА', 'Procedure', 'функция', 'Функция', 'ФУНКЦИЯ', 'Function',
					'конецпроцедуры', 'КонецПроцедуры', 'Конецпроцедуры', 'КОНЕЦПРОЦЕДУРЫ', 'EndProcedure',
					'конецфункции', 'КонецФункции', 'Конецфункции', 'КОНЕЦФУНКЦИИ', 'EndFunction', 'перем',
					'Перем', 'ПЕРЕМ', 'Var', 'перейти', 'Перейти', 'ПЕРЕЙТИ', 'Goto', 'возврат', 'Возврат',
					'ВОЗВРАТ', 'Return', 'продолжить', 'Продолжить', 'ПРОДОЛЖИТЬ', 'Continue', 'прервать',
					'Прервать', 'ПРЕРВАТЬ', 'Break', 'и', 'И', 'And', 'или', 'Или', 'ИЛИ', 'Or', 'не',
					'Не', 'НЕ', 'Not', 'попытка', 'Попытка', 'ПОПЫТКА', 'Try', 'исключение', 'Исключение',
					'ИСКЛЮЧЕНИЕ', 'Except', 'вызватьисключение', 'ВызватьИсключение', 'Вызватьисключение',
					'ВЫЗВАТЬИСКЛЮЧЕНИЕ', 'Raise', 'конецпопытки', 'КонецПопытки', 'Конецпопытки',
					'КОНЕЦПОПЫТКИ', 'EndTry', 'экспорт', 'Экспорт', 'ЭКСПОРТ', 'Export',
		),
		2 => array('Новый', 'New', 'Истина', 'True', 'Ложь', 'False', 'ВРег', 'КодСимвола', 'Лев', 'Найти', 'НРег', 'Прав',
					'ПустаяСтрока', 'Символ', 'СокрЛ', 'СокрП', 'СокрЛП',
					'Сред', 'СтрДлина', 'СтрЗаменить',  'СтрПолучитьСтроку',
					'СтрЧислоВхождений', 'СтрЧислоСтрок', 'Год', 'Year', 'День',
					'Day', 'ДеньГода', 'DayOfYear', 'ДеньНедели', 'DayOfWeek',
					'ДобавитьМесяц', 'AddMonth', 'КонецГода', 'EndOfYear', 'КонецДня',
					'EndOfDay', 'КонецКвартала', 'EndOfQuarter', 'КонецМесяца', 'EndOfMonth',
					'КонецМинуты',  'EndOfMinute', 'КонецНедели', 'EndOfWeek', 'КонецЧаса',
					'EndOfHour', 'Месяц', 'Month', 'Минута', 'Minute', 'НачалоГода', 'BegOfYear',
					'НачалоДня', 'BegOfDay', 'НачалоКвартала', 'BegOfQuarter', 'НачалоМесяца',
					'BegOfMonth', 'НачалоМинуты', 'BegOfMinute', 'НачалоНедели', 'BegOfWeek',
					'НачалоЧаса', 'BegOfHour', 'НеделяГода', 'WeekOfYear', 'Секунда',
					'Second', 'ТекущаяДата', 'CurrentDate', 'Час', 'Hour', 'Булево', 'Boolean',
					'Дата', 'Date', 'Строка', 'String', 'Число', 'Number', 'Формат', 'Format',
					'Вычислить', 'Eval', 'Макс', 'Max', 'Мин', 'Min', 'ОписаниеОшибки',
					'ErrorDescription', 'Тип', 'Type', 'ТипЗнч', 'TypeOf', 'Вопрос',
					'DoQueryBox', 'ОбработкаПрерыванияПользователя', 'UserInterruptProcessing',
					'Оповестить', 'Notify', 'ОткрытьЗначение', 'OpenValue', 'ОткрытьИндексСправки',
					'OpenHelpIndex', 'ОткрытьСодержаниеСправки', 'OpenHelpContent', 'ОткрытьСправку',
					'OpenHelp', 'ОчиститьСообщения', 'ClearMessages',
					'Предупреждение', 'DoMessageBox', 'Сигнал', 'Beep', 'Сообщить',
					'Message', 'Состояние', 'Status',
					'ВвестиДату', 'InputDate', 'ВвестиЗначение', 'InputValue',
					'ВвестиСтроку', 'ВвестиЧисло', 'InputNumber',
					'НСтр', 'NStr', 'ПредставлениеПериода', 'PeriodPresentation',
					'ЧислоПрописью', 'NumberInWords', 'ПолучитьОбщийМакет',
					'GetCommonTemplate', 'ПолучитьОбщуюФорму', 'GetCommonForm',
					'ЗаблокироватьРаботуПользователя', 'LockApplication',
					'ЗавершитьРаботуСистемы', 'Exit',
					'ИмяКомпьютера', 'ComputerName', 'ИмяПользователя', 'UserName',
					'КаталогВременныхФайлов', 'TempFilesDir',
					'КаталогПрограммы', 'BinDir', 'КодЛокализации', 'LocaleCode',
					'ОтключитьОбработчикОжидания', 'DetachIdleHandler',
					'ОтключитьОбработчикОповещения', 'DetachNotificationHandler',
					'ПараметрыДоступа', 'AccessParameters',
					'ПодключитьОбработчикОповещения', 'AttachIdleHandler',
					'ПодключитьОбработчикОповещения', 'AttachNotificationHandler',
					'ПолноеИмяПользователя', 'UserFullName', 'ПолучитьЗаголовокСистемы',
					'GetCaption', 'ПравоДоступа', 'AccessRight',
					'ПрекратитьРаботуСистемы', 'Terminate', 'РольДоступна', 'IsInRole',
					'ACos', 'ASin', 'ATan', 'Cos', 'Exp', 'Log', 'Лог', 'Log10', 'Лог10',
					'Pow', 'Sin', 'Sqrt', 'Tan', 'Окр', 'Round', 'Цел', 'Int'),
		3=> array('БиблиотекаКартинок', 'PictureLib', 'БиблиотекаСтилей', 'StyleLib',
					'ВнешниеОбработки',
					'ExternalProcessings', 'ГлавныйИнтерфейс', 'MainInterface', 'ДокументОбъект',
					'DocumentObject', 'ДокументСсылка',
					'DocumentRef', 'ДокументСписок', 'DocumentList', 'ДокументМенеджер',
					'DocumentManager', 'ЖурналыДокументов', 'DocumentJournals', 'Задачи', 'Tasks',
					'ИспользованиеРабочейДаты', 'WorkingDateUse', 'Константы', 'Constants',
					'КритерииОтбора', 'FilterCriteria', 'ПараметрЗапуска', 'LaunchParameter',
					'ПараметрыСеанса', 'SessionParameters',
					'ПланыВидовРасчета', 'ChartsOfCalculationTypes', 'ПланыВидовХарактеристик',
					'ChartsOfCharacteristicTypes',
					'ПланыСчетов', 'ChartsOfAccounts', 'ПользователиИнформационнойБазы',
					'InfoBaseUsers', 'РабочаяДата', 'WorkingDate',
					'СтатусСообщения',
					'MessageStatus', 'Символы', 'Chars'),
		4 => array('Выбрать', 'Select', 'Выполнить', 'Execute', 'Записать', 'Save',
					'Количество', 'Count', 'НайтиПоНаименованию',
					'FindByDescription', 'НайтиПоКоду', 'FindByCode', 'Следующий', 'Next',
					'СоздатьДокумент', 'СоздатьМенеджерЗаписи', 'CreateRecordManager',
					'УстановитьПараметр'),
		5 => array('справочники', 'Справочники', 'СПРАВОЧНИКИ', 'Catalogs',
					'документы', 'Документы', 'ДОКУМЕНТЫ', 'Documents',
					'константы', 'Константы', 'КОНСТАНТЫ',
					'нумераторы', 'Нумераторы', 'НУМЕРАТОРЫ',
					'перечисления', 'Перечисления', 'ПЕРЕЧИСЛЕНИЯ', 'Enums',
					'регистрысведений', 'РегистрыСведений', 'Регистрысведений', 'РЕГИСТРЫСВЕДЕНИЙ', 'InformationRegisters',
					'регистрынакопления', 'РегистрыНакопления', 'Регистрынакопления', 'РЕГИСТРЫНАКОПЛЕНИЯ', 'AccumulationRegisters',
					'регистрыбухгалтерии', 'РегистрыБухгалтерии', 'Регистрыбухгалтерии', 'AccountingRegisters',
					'регистрырасчета', 'РегистрыРасчета', 'Регистрырасчета', 'РЕГИСТРЫРАСЧЕТА', 'CalculationRegisters', 
					'планыобмена', 'ПланыОбмена', 'Планыобмена', 'ПЛАНЫОБМЕНА', 'ExchangePlans',
					'метаданные', 'Метаданные', 'МЕТАДАННЫЕ', 'Metadata',
					'обработки', 'Обработки', 'ОБРАБОТКИ', 'Processings',
					'отчеты', 'Отчеты', 'ОТЧЕТЫ', 'Reports', 
					'бизнеспроцессы', 'БизнесПроцессы', 'Бизнеспроцессы', 'БИЗНЕСПРОЦЕССЫ', 'BusinessProcesses',
		),
		6 => array('Выбрать', 'выбрать', 'ВЫБРАТЬ', 'Где', 'где', 'ГДЕ', 'Из', 'из', 'ИЗ', 'Подобно', 'подобно', 'ПОДОБНО', 
					'Ссылка', 'ссылка', 'ССЫЛКА',
		),
	),
	'SYMBOLS' => array(
					'(', ')', '[', ']', '{', '}', '!', '@', '%', '&', '*',
					'|', '/', '<', '>', '=', '.', ',', ';', '+', '-',
	),
	'CASE_SENSITIVE' => array(
		GESHI_COMMENTS => false,
		1 => false,
		2 => false,
		3 => false,
		4 => false,
		5 => false,
		6 => false,
	),
	'STYLES' => array(
		'KEYWORDS' => array(
			1 => 'color: red; font-weight: bold;',
			2 => 'color: red;',
			3 => 'color: navy; font-weight: bold;',
			4 => 'color: red;',
			5 => 'color: navy;',
			6 => 'font-weight: bold;',
		),
		'COMMENTS' => array(
			1 => 'color: green; font-style: italic;',
		),
		'ESCAPE_CHAR' => array(
			0 => 'color: #000099; font-weight: bold;'
		),
		'BRACKETS' => array(
			0 => 'color: red;'
		),
		'STRINGS' => array(
			0 => 'color: #006F51;'
		),
		'NUMBERS' => array(
			0 => 'color: blue;'
		),
		'METHODS' => array(
			1 => 'color: green;'
		),
		'SYMBOLS' => array(
			0 => 'color: #a33;'
		),
		'REGEXPS' => array(
		),
		'SCRIPT' => array(
		)
	),
	'URLS' => array(
		1 => '',
		2 => '',
		3 => '',
		4 => '',
		5 => '',
		6 => '',
	),
	'OOLANG' => true,
	'OBJECT_SPLITTERS' => array(
		1 => '.'
	),
	'REGEXPS' => array(
	),
	'STRICT_MODE_APPLIES' => GESHI_NEVER,
	'SCRIPT_DELIMITERS' => array(),
	'HIGHLIGHT_STRICT_BLOCK' => array(),
	'PARSER_CONTROL' => array(
		'KEYWORDS' => array(
			1 => array(
				'DISALLOWED_BEFORE' => '(?<![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
				'DISALLOWED_AFTER' => '(?![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
//				'DISALLOWED_AFTER' => '(?![\w\d])',
			),
			2 => array(
				'DISALLOWED_BEFORE' => '(?<![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
				'DISALLOWED_AFTER' => '(?![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
			),
			3 => array(
				'DISALLOWED_BEFORE' => '(?<![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
				'DISALLOWED_AFTER' => '(?![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
			),
			4 => array(
				'DISALLOWED_BEFORE' => '(?<![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
				'DISALLOWED_AFTER' => '(?![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
			),
			5 => array(
				'DISALLOWED_BEFORE' => '(?<![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
				'DISALLOWED_AFTER' => '(?![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
			),
			6 => array(
				'DISALLOWED_BEFORE' => '(?<![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
				'DISALLOWED_AFTER' => '(?![a-zA-Z0-9]|(\xD0[\x80-\xBF])|\xD1[\x80-\xBF])',
			),
		)
	)
);
?>